module.exports = {
   attributes: {
     deviceId: {
       type: 'string'
     },
     team: {
       type: 'string',
       enum: ['Instinct', 'Mystic', 'Valor']
     },
     reputation: {
       type: 'integer',
       default: 0
     },
     spawns: {
       collection: 'spawn',
       via: 'owner'
     },
     lastLocation: {
       type: 'json'
     }
   },
   beforeCreate: function (values, cb) {
    values.lastLocation = [values.lastLocation.lng, values.lastLocation.lat];
    cb();
  },
  beforeUpdate: function (values, cb) {
    values.lastLocation = [values.lastLocation.lng, values.lastLocation.lat];
    cb();
  },
 }
