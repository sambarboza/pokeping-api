module.exports = {
   attributes: {
     pokemonId: {
       type: 'integer'
     },
     location: {
       type: 'json',
     },
     score: {
       type: 'integer',
       defaultsTo: 0
     },
     owner: {
       model: 'user'
     }
   },
   beforeCreate: function (values, cb) {
    values.location = [values.location.lng, values.location.lat];
    cb();
   }
 }
